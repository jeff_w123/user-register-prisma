import { Pagination } from "../data/models";

export const getBaseUrl = (reqUrl: string) =>
    reqUrl.substring(
        0, 
        reqUrl.indexOf('?') > -1 
            ? reqUrl.indexOf('?') 
            : reqUrl.length
      );

export const getPaginationInfo = (page: number | undefined, pageSize: number | undefined): Pagination => {
    const take = Number(pageSize)
        ? Number(pageSize) 
        : takeDefault;

    const skip = Number(page)
        ? ((Number(page) - 1) * take)
        : skipDefault;

    return {skip, take}
}

export const getPaginationHeaders = (take: number, skip: number, count: number, reqUrl: string): string[] [] => {
    const baseUrl =
        reqUrl.substring(
            0, 
            reqUrl.indexOf('?') > -1 
                ? reqUrl.indexOf('?') 
                : reqUrl.length
          )
    
    const prevPage = skip / take;
    const currentPage = prevPage + 1;
    const nextPage = currentPage + 1;

    const prevPageLink = prevPage && (prevPage * take < count) 
        ? `${baseUrl}?page=${prevPage}&pageSize=${take}`
        : '';
    
    const nextPageLink = currentPage * take < count
        ? `${baseUrl}?page=${nextPage}&pageSize=${take}`
        : '';

    return [
        ['X-Pagination-TotalCount', count.toString()],
        ['X-Pagination-PreviousPage', prevPageLink],
        ['X-Pagination-NextPage', nextPageLink]
    ];
}

export const takeDefault = 10;

export const skipDefault = 0;