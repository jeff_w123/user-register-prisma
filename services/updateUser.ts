import { addUser, updateUser } from '../data/usersRepository';
import { UserToUpdateDto } from '../data/models';


export default async (userToUpdate: UserToUpdateDto, id: number) => await updateUser(userToUpdate, id);