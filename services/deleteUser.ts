import { deleteUser } from '../data/usersRepository';


export default async (id: number) => await deleteUser(id);