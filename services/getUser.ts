import { getUser, searchUsers } from '../data/usersRepository';
import { PaginatedResult, Pagination, UserToGet } from '../data/models';


export default async (id: number): Promise<UserToGet | null> => 
    await getUser(id);