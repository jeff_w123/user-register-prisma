import { searchUsers } from '../data/usersRepository';
import { PaginatedResult, Pagination, UserToGet } from '../data/models';


export default async ({take, skip}: Pagination, lastName: string | undefined): Promise<PaginatedResult<UserToGet>> => {
    
    // query
    const where = lastName
        ? { lastName: { contains: lastName.toString()} }
        : {};

    const [results, count] = await searchUsers(where, { take, skip });
    return {results, count};
}