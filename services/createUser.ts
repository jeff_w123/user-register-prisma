import { addUser } from '../data/usersRepository';
import { UserToCreateDto } from '../data/models';


export default async (userToCreate: UserToCreateDto) => await addUser(userToCreate);