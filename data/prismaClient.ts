import { Prisma, PrismaClient } from '@prisma/client';
import { Pagination } from './models';


const getExtendedClient = () => 
  new PrismaClient().$extends({
    model: {
      $allModels: {
        // generic pagination for all models
        async fetchPaginated<T>(
          this: T,
          where: object,
          { take, skip }: Pagination,
          // select is optional: undefined returns all columns
          select?: object
        ): Promise<any[]> {
          // Get the current model at runtime
          const context = Prisma.getExtensionContext(this);
          return await Promise.all([
            (context as any).findMany({ where, select, take, skip }),
            (context as any).count({ where }),
          ]);
        },
      },
    },
    query: {
      $allModels: {
        async create({ model, operation, args, query }) {
          // select is optional: undefined returns only the resource id
          args.select = args.select ? args.select : { id: true} 
          return query(args)
        },      
      },
    },
  });

type ExtendedPrismaClient = ReturnType<typeof getExtendedClient>

// Create a singleton function for Prisma Client
const singleton = () => {

  if (!globalThis.prismaGlobal) {
    console.log('Creating prisma client...')
    globalThis.prismaGlobal = getExtendedClient();
  }
  
  return globalThis.prismaGlobal;
};

// Declare global variable prismaGlobal
declare global { var prismaGlobal: ExtendedPrismaClient | undefined; }

const prisma = singleton();

export default prisma;