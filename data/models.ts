export interface Pagination {
    take: number;
    skip: number;
};

/**
 * User to be created.
 */
export interface UserToCreateDto {
    /**
     * An email address in a valid format.
     * @pattern ^(.+)@(.+)$ Email address needs to be in a valid format.
     */
    email: string;
    firstName: string;
    lastName: string;
};

/**
 * User to update.
 */
export interface UserToUpdateDto extends UserToCreateDto {};

/**
 * User to get.
 */
export interface UserToGet extends UserToCreateDto {
    id: number;
};

/**
 * @minimum 1
 */
export type PageDto = number;

/**
 * @minimum 1
 * @maximum 250
 */
export type PageSizeDto = number;

export interface PaginatedResult<T> {
    results: T[];
    count: number;
};

export interface CreatedId { id: number };