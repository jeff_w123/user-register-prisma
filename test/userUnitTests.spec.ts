import { expect } from 'chai';

import { getBaseUrl, getPaginationHeaders, getPaginationInfo, skipDefault, takeDefault } from '../utilities/common';


describe('Unit Tests', () => { 
  
  it('getBaseUrl without query should return url', () => {
    const inputUrl: string = '/api/v1/users'
    const result = getBaseUrl(inputUrl)
    expect(result).equal(inputUrl)
  });
  
  it('getBaseUrl with query should return url', () => {
    const inputUrl: string = '/api/v1/users?page=2'
    const expectedUrl: string = '/api/v1/users'
    const result = getBaseUrl(inputUrl)
    expect(result).equal(expectedUrl)
  });
  
  it('getPaginationInfo with undefined inputs should return defaults', () => {
    const {skip, take} = getPaginationInfo(undefined, undefined)
    expect(skip).equal(skipDefault)
    expect(take).equal(takeDefault)
  });
  
  it('getPaginationInfo with valid inputs should return correct pagination info', () => {
    const {skip, take} = getPaginationInfo(3, 10)
    expect(skip).equal(20)
    expect(take).equal(10)
  });
  
  it('getPaginationHeaders with first page inputs should return correct headers', () => {
    const headersStrings = getPaginationHeaders(10, 0, 25, '/api/v1/users')
    expect(headersStrings.toString())
      .equal([
          ['X-Pagination-TotalCount', '25'],
          ['X-Pagination-PreviousPage', ''],
          ['X-Pagination-NextPage', '/api/v1/users?page=2&pageSize=10']
        ].toString()
      )
  });
  
  it('getPaginationHeaders with middle page inputs should return correct headers', () => {
    const headersStrings = getPaginationHeaders(10, 10, 25, '/api/v1/users')
    expect(headersStrings.toString())
      .equal([
          ['X-Pagination-TotalCount', '25'],
          ['X-Pagination-PreviousPage', '/api/v1/users?page=1&pageSize=10'],
          ['X-Pagination-NextPage', '/api/v1/users?page=3&pageSize=10']
        ].toString()
      )
  });
  
  it('getPaginationHeaders with last page inputs should return correct headers', () => {
    const headersStrings = getPaginationHeaders(10, 20, 25, '/api/v1/users')
    expect(headersStrings.toString())
      .equal([
          ['X-Pagination-TotalCount', '25'],
          ['X-Pagination-PreviousPage', '/api/v1/users?page=2&pageSize=10'],
          ['X-Pagination-NextPage', '']
        ].toString()
      )
  });

});

  