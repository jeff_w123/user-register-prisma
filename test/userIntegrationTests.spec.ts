import { expect } from 'chai';
import request from 'supertest';

import { addUser, addUsers, deleteAllUsers } from '../utilities/test';


describe('Integration Tests: Create user', () => { 
  
  // runs once before the first test in this block
  before(async () => { await deleteAllUsers(); });

  // runs after each test in this block
  afterEach(async () => { await deleteAllUsers(); });

  it('should create a user when the input is valid', async () => {
    const response = await request(`http://${process.env.DOMAIN_URL}:${process.env.API_PORT}/`)
      .post('api/v1/users')
      .send({
        firstName: "tony",
        lastName: "Tiger",
        email:"tony@hotmail.com"
      })
      .set('Accept', 'application/json')
      expect(response.status).equal(201)
  });

  it('should return an error when firstName is missing', async () => {
    const response = await request(`http://${process.env.DOMAIN_URL}:${process.env.API_PORT}/`)
      .post('api/v1/users')
      .send({
        //firstName: "tony", first name missing
        lastName: "Tiger",
        email:"tony@hotmail.com"
      })
      .set('Accept', 'application/json')
      expect(response.status).equal(422)
      expect(response.body.error).not.equal(null);
  });

  it('should return an error when email address is not unique', async () => {
    const userEmail = 'harry@gmail.com';
    await addUser(userEmail);
    const response = await request(`http://${process.env.DOMAIN_URL}:${process.env.API_PORT}/`)
      .post('api/v1/users')
      .send({
        firstName: "george",
        lastName: "Hamster",
        email: userEmail // duplicate email
      })
      .set('Accept', 'application/json')
      expect(response.status).equal(500)
      expect(response.body.error).not.equal(null);
  });
});

describe('Integration Tests: Search users', () => { 
  
  // runs once before the first test in this block
  before(async () => { await deleteAllUsers(); });

  // runs before each test in this block
  beforeEach(async () => { await addUsers(); });

  // runs after each test in this block
  afterEach(async () => { await deleteAllUsers(); });

  it('should return a user when search input is valid', async () => {
    const response = await request(`http://${process.env.DOMAIN_URL}:${process.env.API_PORT}/`)
      .get('api/v1/users?lastName=Rabb')
      .set('Accept', 'application/json')
      expect(response.status).equal(200)
      expect(response.body.length).equal(1)
      expect(response.body[0]?.firstName).equal("Rebecca")
      expect(response.body[0]?.lastName).equal("Rabbit")
      expect(response.body[0]?.email).equal("rebecca@spark.com");
  });

  it('should return full list when no search entered', async () => {
    const response = await request(`http://${process.env.DOMAIN_URL}:${process.env.API_PORT}/`)
      .get('api/v1/users')
      .set('Accept', 'application/json')
      expect(response.status).equal(200)
      expect(response.body.length).equal(6)
  });
});

  