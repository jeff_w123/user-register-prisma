import { addAsync } from '@awaitjs/express';
import express from "express";
import http from "http";
import parser from "body-parser";
import swaggerUi from "swagger-ui-express";
import { Response as ExResponse, Request as ExRequest } from "express";

import { RegisterRoutes } from './presentation/routes';
import { errorHandler } from './utilities/errors';
import prisma from './data/prismaClient';


const app = express();
const router = addAsync(app);

router.use(parser.urlencoded({ extended: true }));
router.use(parser.json());
RegisterRoutes(app);

// api docs route
app.use("/api-docs", swaggerUi.serve, async (_req: ExRequest, res: ExResponse) => {
  return res.send(
    swaggerUi.generateHTML(await import("./presentation/swagger.json"))
  );
});

// undocumented route for docker compose health check
router.use('/health', async (req, res, next) =>
    await prisma.$queryRaw`SELECT 1`
    .then((result: any) => res.status(200).json({ message: 'I am healthy!'}))
    .catch((e: Error) => {
      const message = `Health check failed.`
      console.log(`${message} Cannot connect to the database: ${e}`);
      return res.status(500).json({ message })
    })
);

const server = http.createServer({}, router);
server.listen(process.env.API_PORT, () => {
    console.log(`Server is running at http://${process.env.DOMAIN_URL}:${process.env.API_PORT}`)
});

app.use(errorHandler);